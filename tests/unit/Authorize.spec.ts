import chai, {expect} from 'chai';
import sinon from 'sinon';
import { shallowMount } from '@vue/test-utils';
import Authorize from '@/components/Authorize.vue';


describe('Authorize.vue', () => {
  it('redirects with a checkout key', () => {
    const $route = {
      query: {
        checkout_key: 'test',
      },
    };

    const redirectStub = sinon.stub();

    const wrapper = shallowMount(Authorize, {
      mocks: {
        $route,
      },
      methods: {
        redirect: redirectStub,
      },
    });
    expect(redirectStub.called).to.be.true;
  });

  it('redirects without a checkout key', () => {
    const $route = {
        query: {},
    };

    const redirectStub = sinon.stub();

    const wrapper = shallowMount(Authorize, {
      mocks: {
        $route,
      },
      methods: {
        redirect: redirectStub,
      },
    });
    expect(redirectStub.called).to.be.true;
  });
});
