import { expect } from 'chai';
import { shallowMount } from '@vue/test-utils';
import Splash from '@/components/Splash.vue';

describe('Splash.vue', () => {
  it('renders splash logo', () => {
    const wrapper = shallowMount(Splash);
    expect(wrapper.find('#back-nav')).to.exist;
  });
});
