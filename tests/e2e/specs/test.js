// https://docs.cypress.io/api/introduction/api.html

describe('Splash loads ok', () => {
  it('Visits the splash screen for a basic sanity test', () => {
    cy.visit("/splash");
    cy.get("#back-nav").should("have.text", "Go back");
  })
})