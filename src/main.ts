import Vue from 'vue';
import VueRouter from 'vue-router';
import './stylesheets/main.scss';
import App from './App.vue';

import Authorize from './components/Authorize.vue';
import Splash from './components/Splash.vue';
import SendWyre from './components/SendWyre.vue';
import StripeComponent from './components/Stripe.vue';

const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '/', component: SendWyre },
    { path: '/splash', component: Splash},
    { path: '/authorize', component: Authorize},
    { path: '/stripe', component: StripeComponent },
    { path: '*', component: Splash },
  ],
});

Vue.config.productionTip = false;
Vue.use(VueRouter);
new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');

