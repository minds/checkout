export default interface Account {
    userGuid: string;
    accountId: string;
}
