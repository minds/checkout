import Account from './Account';
export default interface AccountResponse {
    status: string;
    account: Account;
}
